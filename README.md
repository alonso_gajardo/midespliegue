# MOODLE API

_Bienvenido a MOODLE API para el programa de atraccion de talentos en la UDP, aca encontraras la informacion nesesaria para poder interactuar con esta API._

## IMPORTANTE

_Todas las peticiones correctas tienen el estado HTTP 200, el resto seran consideradas erroneas o inconsistentes._


## Cursos

_En el presente apartado se veran las rutas para poder obtener información referente a los cursos de moodle._


### getCourse

_Ruta para poder obtener (GET) el ID, Fullname y Timeend. Recibiendo como parametro el correo de un usuario (campo obligatorio)._


HTTP Request (GET)
```
/getCourse
```

Parametros

* email (string, obligatorio, unico): Recibe un (y solo uno) email como parametro devolviendo los cursos asociados a este, como un arreglo de objetos JSON.

Ejemplos

PETICION 1: (GET)

 ```
 /getCourse?email=MattPrueba@example.c
 ```

```json

[
    {
        "Course_id": 4,
        "Fullname": "Game_Course2",
        "Timeend": 1608317400
    }
]

```

PETICION 2: (GET)

```
/getCourse?email=M 
```

HTTP 204 No content.


## Construido con 🛠

_Esta API fue programada usando las siguientes herramientas_

* [Golang](https://golang.org/) - El lenguaje utilizado.
* [Gorm](https://gorm.io/) - Para interactuar con la BD.