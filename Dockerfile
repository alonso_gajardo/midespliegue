FROM golang:1.13-alpine3.11 as builder
RUN mkdir /build
ADD . /build/
WORKDIR /build
RUN apk add git
RUN go get -u gorm.io/gorm
RUN go get gorm.io/driver/mysql
RUN go build -o app main.go


FROM frolvlad/alpine-glibc
#FROM debian
RUN apk update \
        && apk upgrade \
        && apk add --no-cache \
        ca-certificates \
        && update-ca-certificates 2>/dev/null || true



COPY --from=builder /build/app  /app/app
EXPOSE 8000

ENTRYPOINT ["/app/app"]
