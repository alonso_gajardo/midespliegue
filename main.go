package main

import (
	"log"
	"net/http"

	"./router"
)

func main() {
	http.HandleFunc("/getCourse", router.GetCourse)
	log.Fatal(http.ListenAndServe(":8000", nil))
}
