package router

import(
	"log"
	"net/http"
	"encoding/json"
	"../db"
)

type GetCourseResult struct {
    Course_id uint64
    Fullname  string
    Timeend   uint64
}

func GetCourse(w http.ResponseWriter, r *http.Request) { //GET: email como parametro
	if (r.Method != "GET"){
		log.Println("400: Bad request in GetCouse.")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	email, ok := r.URL.Query()["email"]
	if(!ok || len(email) < 1){
		log.Println("204: No content in GetCourse.")
		w.WriteHeader(http.StatusNoContent)
		return
	}else if(len(email) > 1){
		log.Println("414: Request uri too long in GetCourse.")
		w.WriteHeader(http.StatusRequestURITooLong)
		return
	}
	query := `Select  mdl_course.id as course_id, mdl_course.fullname, mdl_user_enrolments.timeend
	 FROM mdl_user join  mdl_user_enrolments on mdl_user.id=mdl_user_enrolments.userid  join  mdl_enrol on mdl_user_enrolments.enrolid=mdl_enrol.id
		join  mdl_course on mdl_enrol.courseid=mdl_course.id WHERE mdl_user.email=?`
	var Result []GetCourseResult
	db.DB.Raw(query, email[0]).Scan(&Result)
	if (Result == nil){
		log.Println("204: No content in GetCourse.")
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	log.Println("200: OK in GetCourse.")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(&Result)
}
